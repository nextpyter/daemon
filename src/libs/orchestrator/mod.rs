use std::{env, fmt::{Debug, Display}};

use axum::async_trait;
use futures::{StreamExt, TryStreamExt};
use k8s_openapi::{api::core::v1::PersistentVolumeClaim, NamespaceResourceScope};
use kube::{api::{WatchEvent, WatchParams}, Api};
use serde::de::DeserializeOwned;
use thiserror::Error;

use super::types::notebook_description::NotebookDescription;

pub mod kubernetes;
pub mod docker;
pub mod errors;
pub mod waitable;

#[derive(Error, Debug)]
pub enum OrchestratorError {
    #[error("docker error: {0}")]
    DockerError(bollard::errors::Error),
    #[error("fatal error: {0}")]
    Generic(String),
    #[error("couldn't serialize: {0}")]
    SerializationError(String),
    #[error("kubernetes error: {0}")]
    KubernetesError(String),
}

pub fn get_k8s_storage_node_name() -> String {
    std::env::var("K8S_STORAGE_NODE_NAME")
        .unwrap()
}

pub fn get_k8s_notebooks_storage_class() -> String {
    std::env::var("K8S_NOTEBOOK_SC")
        .unwrap()
}

pub fn get_mountpath() -> String {
    std::env::var("BIND_VOLUME_PATH")
        .unwrap()
}

pub fn get_k8s_dataset() -> String {
    std::env::var("K8S_DATASET")
        .unwrap()
}

pub fn get_k8s_namespace() -> String {
    std::env::var("K8S_NAMESPACE")
        .unwrap()
}

pub fn get_k8s_pv() -> String {
    std::env::var("K8S_PERSISTENT_VOLUME_NAME")
        .unwrap()
}

pub fn build_volume_name(
    container_name: &str
) -> String {
    format!("{}-volume", container_name)
}

pub fn build_notebook_name(
    identifier: &str
) -> String {
    format!("notebook-{}", identifier)
}

pub fn build_notebook_uri(
    container_name: &str,
    filename: Option<&str>
) -> String {
    if filename != None {
        format!("{}/notebook/{}/lab/tree/RTC:{}", env::var("PROXY_URI").unwrap(), container_name, filename.unwrap())
    }else{
        format!("{}/notebook/{}/lab", env::var("PROXY_URI").unwrap(), container_name)
    }
}

#[async_trait]
pub trait Orchestrator: Send + Sync {

    async fn create_notebook_volume(
        &self, 
        container_name: &str, 
        path: &str
    ) -> Result<String, OrchestratorError>;

    async fn remove_notebook_volume(
        &self, 
        volume_name: &str, 
    ) -> Result<(), OrchestratorError>;

    async fn start_notebook_container(
        &self, 
        container_name: &str,
        container_image: &str,
        container_command: Vec<&str>,
        volume_name: &str,
        filename: Option<&str>,
    ) -> Result<String, OrchestratorError>;

    async fn get_all_notebook_containers_paginated(
        &self, 
    ) -> Result<Vec<NotebookDescription>, OrchestratorError>;

    async fn get_notebook_container(
        &self,
        container_name: &str,
    ) -> Result<Option<NotebookDescription>, OrchestratorError>;

    async fn stop_notebook_container(
        &self,
        container_name: &str
    ) -> Result<(), OrchestratorError>;

    async fn restart_notebook_container(
        &self,
        container_name: &str,
    ) -> Result<(), OrchestratorError>;

    async fn delete_notebook_container(
        &self,
        container_name: &str,
    ) -> Result<(), OrchestratorError>;

    async fn delete_notebook_container_data(
        &self,
        container_volume_name: &str,
    ) -> Result<(), OrchestratorError>;

}