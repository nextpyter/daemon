use std::{collections::HashMap, env, path::PathBuf};

use super::{
    build_notebook_uri, build_volume_name, Orchestrator, OrchestratorError,
};
use crate::libs::types::notebook_description::NotebookDescription;
use axum::async_trait;
use bollard::{
    container::{
        Config, CreateContainerOptions, ListContainersOptions,
        StartContainerOptions,
    },
    network::ConnectNetworkOptions,
    secret::HostConfig,
    volume::{CreateVolumeOptions, ListVolumesOptions, RemoveVolumeOptions},
    Docker,
};
use log::warn;

#[derive(Clone)]
pub struct DockerOrchestrator {
    pub runtime: Docker,
}

impl DockerOrchestrator {
    pub fn new(proxied_docker: bool) -> Self {
        // feat: add support for docker proxy
        if proxied_docker {
            let docker_host = env::var("DOCKER_HOST").unwrap(); // this must be defined at this point!
            tracing::info!("starting with docker proxy: {docker_host}");
            // uses the DOCKER_HOST env variable to connect
            let docker = Docker::connect_with_defaults()
                .expect("cannot connect to docker");
            DockerOrchestrator { runtime: docker }
        } else {
            let docker = Docker::connect_with_socket_defaults()
                .expect("cannot connect to docker");
            DockerOrchestrator { runtime: docker }
        }
    }
}

#[async_trait]
impl Orchestrator for DockerOrchestrator {
    async fn create_notebook_volume(
        &self,
        container_name: &str,
        path: &str,
    ) -> Result<String, OrchestratorError> {
        let volume_name = build_volume_name(container_name);
        let mut driver_opts = HashMap::new();
        let base_path = env::var("BIND_VOLUME_PATH").unwrap();
        let mut full_path = PathBuf::new();
        full_path.push(base_path);
        full_path.push(path);
        let full_path = String::from(full_path.to_string_lossy());
        dbg!(&full_path);
        driver_opts.insert("device".to_string(), full_path);
        driver_opts.insert("o".to_string(), "bind".to_string());
        driver_opts.insert("type".to_string(), "none".to_string());

        let volume_config = CreateVolumeOptions {
            name: volume_name.clone(),
            driver_opts,
            ..Default::default()
        };

        self.runtime.create_volume(volume_config).await?;

        Ok(volume_name)
    }

    async fn remove_notebook_volume(
        &self,
        volume_name: &str,
    ) -> Result<(), OrchestratorError> {
        self.runtime
            .list_volumes(Some(ListVolumesOptions {
                filters: HashMap::from([("name", vec![volume_name])]),
                ..Default::default()
            }))
            .await?;

        Ok(())
    }

    async fn start_notebook_container(
        &self,
        container_name: &str,
        container_image: &str,
        container_command: Vec<&str>,
        volume_name: &str,
        filename: Option<&str>,
    ) -> Result<String, OrchestratorError> {
        tracing::info!("binding to {volume_name}");
        let network_name = env::var("DOCKER_NETWORK").unwrap().to_string();
        let container_options = Some(CreateContainerOptions {
            name: container_name,
            platform: None, // leaves docker up to defining it
        });
        let container_config = Config {
            hostname: Some(container_name),
            image: Some(container_image),
            env: Some(vec![
                "NB_USER=nextpyter",
                "NB_GID=33",
                "NB_UMASK=002",
                "CHOWN_HOME=yes",
            ]),
            user: Some("root"),
            working_dir: Some("/home/nextpyter"),
            labels: Some(HashMap::from([
                ("taint", "nextpyter-notebook"), // this way we can retrieve all containers started by this daemon
                ("notebook-name", container_name),
            ])),
            cmd: Some(container_command),
            tty: Some(false),
            host_config: Some(HostConfig {
                network_mode: Some(network_name.clone()),
                binds: Some(vec![
                    // TODO: dynamic mount of the collaboration scripts
                    // "/home/macca/stuff/uni/tirocinio/daemon/pre-notebook/install-collaboration.sh:/usr/local/bin/before-notebook.d/install-jupyter-collaboration.sh".to_string(),
                    format!("{}:/home/nextpyter", volume_name),
                ]),
                group_add: Some(vec!["users".to_string()]),
                // auto_remove: Some(true),
                ..HostConfig::default()
            }),
            ..Default::default()
        };

        self.runtime
            .create_container(container_options, container_config)
            .await?;

        self.runtime
            .connect_network(
                &network_name,
                ConnectNetworkOptions {
                    container: container_name,
                    ..Default::default()
                },
            )
            .await?;

        self.runtime
            .start_container(
                container_name,
                None::<StartContainerOptions<String>>,
            )
            .await?;
        Ok(build_notebook_uri(container_name, filename))
    }

    async fn get_all_notebook_containers_paginated(
        &self,
    ) -> Result<Vec<NotebookDescription>, OrchestratorError> {
        let filters = HashMap::from([
            ("label", vec!["taint=nextpyter-notebook"]), // all notebooks have this taint
            ("status", vec!["running", "paused", "exited", "dead"]),
        ]);
        /*if let Some(container_id) = from {
            filters.insert("since", vec!(container_id));
        }*/

        let containers = self
            .runtime
            .list_containers(Some(ListContainersOptions {
                // limit: Some(15), // paginated results
                filters,
                ..Default::default()
            }))
            .await?;

        let result: Vec<NotebookDescription> = containers
            .into_iter()
            .map(|container| container.try_into())
            .collect::<Result<Vec<NotebookDescription>, _>>()?;

        Ok(result)
    }

    async fn get_notebook_container(
        &self,
        container_name: &str,
    ) -> Result<Option<NotebookDescription>, OrchestratorError> {
        let result = self
            .runtime
            .list_containers(Some(ListContainersOptions {
                filters: HashMap::from([
                    ("name", vec![container_name]),
                    ("status", vec!["running", "paused", "exited", "dead"]), // let's search for stopped containers also
                ]),
                ..Default::default()
            }))
            .await?;

        if result.len() == 0 {
            return Ok(None);
        }
        if result.len() > 1 {
            warn!("{container_name} has been found multiple times?");
        }

        let found_container: NotebookDescription =
            result.first().unwrap().try_into()?; // there might be more results

        Ok(Some(found_container))
    }

    async fn stop_notebook_container(
        &self,
        container_name: &str,
    ) -> Result<(), OrchestratorError> {
        self.runtime.stop_container(container_name, None).await?;
        Ok(())
    }

    async fn restart_notebook_container(
        &self,
        container_name: &str,
    ) -> Result<(), OrchestratorError> {
        self.runtime
            .restart_container(&container_name, None)
            .await?;
        Ok(())
    }

    async fn delete_notebook_container(
        &self,
        container_name: &str,
    ) -> Result<(), OrchestratorError> {
        self.runtime.remove_container(container_name, None).await?;
        Ok(())
    }

    async fn delete_notebook_container_data(
        &self,
        container_volume_name: &str,
    ) -> Result<(), OrchestratorError> {
        self.runtime
            .remove_volume(
                container_volume_name,
                Some(RemoveVolumeOptions { force: true }),
            )
            .await?;

        Ok(())
    }
}
