use axum::async_trait;
use k8s_openapi::api::core::v1::{PersistentVolumeClaim, Pod};
use kube::{api::{DeleteParams, ListParams, PostParams}, Api, Client};
use serde_json::json;
use crate::libs::{orchestrator::{build_volume_name, get_k8s_notebooks_storage_class, get_k8s_storage_node_name, get_mountpath}, types::notebook_description::{self, NotebookDescription}};
use super::{build_notebook_uri, get_k8s_dataset, get_k8s_namespace, waitable::{impl_pod::PodStatusEnum, Waitable, WaitableKubernetesResource}, Orchestrator, OrchestratorError};

#[derive(Clone)]
pub struct KubernetesOrchestrator {
    pub runtime: kube::Client,

}

impl KubernetesOrchestrator {
    pub async fn new() -> Self {
        let runtime = Client::try_default().await.expect("cannot connect to kubernetes api server");
        KubernetesOrchestrator { runtime }
    }
}

#[async_trait]
impl Orchestrator for KubernetesOrchestrator {

    async fn create_notebook_volume(
        &self,
        _container_name: &str,
        path: &str
    ) ->  Result<String, OrchestratorError> {
        /*let claim_name = build_volume_name(container_name);
        let persistent_volume_claim: Api<PersistentVolumeClaim> = Api::namespaced(
            self.runtime.clone(),
            &get_k8s_namespace()
        );
        let definition: PersistentVolumeClaim = serde_json::from_value(json!({
            "apiVersion": "v1",
            "kind": "PersistentVolumeClaim",
            "metadata": { 
                "name": claim_name,
                "annotations": {
                    "volume.kubernetes.io/selected-node": get_k8s_storage_node_name(),
                }
            },
            "spec": {
                "storageClassName": get_k8s_notebooks_storage_class(),
                "accessModes": ["ReadWriteOnce"],
                "resources": {
                    "requests": {
                        "storage": "10Mi" // space given to each notebook
                    }
                }
            }
        }))?;
        let post_params = PostParams::default();
        persistent_volume_claim
            .create(&post_params, &definition)
            .await?;
        let waitable = WaitableKubernetesResource::new(persistent_volume_claim);
        waitable.wait(&format!("metadata.name={}", claim_name), "Pending").await?;*/
        /*  
            we cannot specify the bind in the PersistentVolumeClaim definition,
            because the subPath bind is done inside the Pod definition file.
            >> https://kubernetes.io/docs/concepts/storage/volumes/#using-subpath 
         */
        Ok(format!("{}:{}", get_k8s_dataset(), path))
    }

    async fn remove_notebook_volume(
        &self, 
        volume_name: &str, 
    ) -> Result<(), OrchestratorError> {
        let persistent_volume_claim: Api<PersistentVolumeClaim> = Api::namespaced(
            self.runtime.clone(),
            &get_k8s_namespace()
        );
        
        persistent_volume_claim.delete(volume_name, &DeleteParams::default()).await?;

        Ok(())
    }

    async fn start_notebook_container(
        &self, 
        container_name: &str,
        container_image: &str,
        container_command: Vec<&str>,
        volume_name: &str,
        filename: Option<&str>,
    ) -> Result<String, OrchestratorError>{

        let pod_api: Api<Pod> = Api::namespaced(
            self.runtime.clone(), 
            &get_k8s_namespace()
        );

        /*
            since we cannot mount volumes when creating the the notebook volume (in k8s)
            we need to do this thing
            TODO: rework interface?
        */

        let split: Vec<&str> = volume_name.split(":").collect();
        if split.len() != 2 {
            return Err(
                OrchestratorError::Generic("invalid volume name".to_string())
            )
        }

        let pod_definition: Pod = serde_json::from_value(json!({
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": { 
                "name": container_name,
                "notebook-name": container_name, // let's be consistent with the docker naming
                "namespace": get_k8s_namespace(),
                "labels": {
                    "dataset.0.id": split[0],
                    "dataset.0.useas": "mount",
                    "taint": "nextpyter-notebook",
                }
            },
            "spec": {
                "hostname": container_name,
                "subdomain": "nextpyter-headless-service",
                "volumes": [
                    {
                        "name": "notebook-volume",
                        "persistentVolumeClaim": {
                            "claimName": split[0]
                        }
                    }
                ],
                "containers": [{
                    "name": container_name,
                    "image": container_image,
                    "command": container_command,
                    "imagePullPolicy": "IfNotPresent",
                    "ports": [{
                        "containerPort": 15750,
                        "name": "traffic"
                    }],
                    "workingDir": "/home/nextpyter",
                    "tty": false,
                    "volumeMounts": [
                        {
                            "mountPath": "/home/nextpyter",
                            "name": "notebook-volume",
                            "subPath": split[1],
                        }
                    ],
                    "env": [
                        {
                            "name": "NB_USER",
                            "value": "nextpyter"
                        },
                        {
                            "name": "NB_GID",
                            "value": "33"
                        },
                        {
                            "name": "NB_UMASK",
                            "value": "002"
                        },
                        {
                            "name": "CHOWN_HOME",
                            "value": "yes"
                        }
                    ]
                }] 
            }
        }))?;

        pod_api
            .create(&PostParams::default(), &pod_definition)
            .await?;

        let waitable_pod = WaitableKubernetesResource::new(pod_api);

        // https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#pod-phase
        waitable_pod.wait(&format!("metadata.name={}", container_name), PodStatusEnum::Running)
            .await?;

        // we create the pod but we don't do anything more, we just let kubernetes
        // do its thing

        Ok(build_notebook_uri(container_name, filename))

    }

    async fn get_all_notebook_containers_paginated(
        &self, 
    ) -> Result<Vec<NotebookDescription>, OrchestratorError>{
        let pod_api: Api<Pod> = Api::namespaced(
            self.runtime.clone(), 
            &get_k8s_namespace()
        );

        let list = pod_api
            .list(&ListParams::default().labels("taint=nextpyter-notebook"))
            .await?
            .items;

        let mapped = match list
            .into_iter()
            .map(|i| i.try_into())
            .collect::<Result<Vec<NotebookDescription>, _>>() {
                Ok(result) => Ok(result),
                Err(_) => Err(OrchestratorError::Generic("cannot convert into notebook desc".to_string()))
        }?;
            
        Ok(mapped)
    }

    async fn get_notebook_container(
        &self, 
        container_name: &str,
    ) -> Result<Option<NotebookDescription>, OrchestratorError>{
        let pod_api: Api<Pod> = Api::namespaced(
            self.runtime.clone(), 
            &get_k8s_namespace()
        );
        let pod = pod_api.get_opt(&container_name).await?;
        dbg!(&pod);
        if let None = pod {
            return Ok(None)
        }

        Ok(Some(pod.unwrap().try_into()?))
    }

    async fn stop_notebook_container(
        &self,
        container_name: &str
    ) -> Result<(), OrchestratorError>{
        // pods cannot be stopped in kubernetes, so we just delete it
        self.delete_notebook_container(container_name).await?;
        Ok(())
    }

    async fn restart_notebook_container(
        &self,
        container_name: &str,
    ) -> Result<(), OrchestratorError>{
        let pod_api: Api<Pod> = Api::namespaced(
            self.runtime.clone(), 
            &get_k8s_namespace()
        );
        let pod = pod_api.get(&container_name).await?;
        pod_api.replace(
            container_name,
            &PostParams::default(), 
            &pod
        ).await?;
        Ok(())
    }

    async fn delete_notebook_container(
        &self,
        container_name: &str,
    ) -> Result<(), OrchestratorError>{
        let pod_api: Api<Pod> = Api::namespaced(
            self.runtime.clone(), 
            &get_k8s_namespace()
        );
        pod_api.delete(&container_name, &DeleteParams::default()).await?;
        Ok(())
    }

    async fn delete_notebook_container_data(
        &self,
        container_volume_name: &str,
    ) -> Result<(), OrchestratorError>{
        // pods read from a pvc, we can't "delete" pod data because it's abstracted
        // by the pvc itself
        Ok(())
    }
}