use axum::async_trait;
use futures::{StreamExt, TryStreamExt};
use k8s_openapi::api::core::v1::{PersistentVolumeClaim, Pod};
use kube::{api::{WatchEvent, WatchParams}, Api};

use crate::libs::orchestrator::OrchestratorError;
use super::{Waitable, WaitableKubernetesResource};

// https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#pod-phase
pub enum PodStatusEnum {
    Running,
    Succeeded,
    Failed,
    Unknown,
    Pending
}
impl ToString for PodStatusEnum {
    fn to_string(&self) -> String {
        match self {
            PodStatusEnum::Running => String::from("Running"),
            PodStatusEnum::Succeeded => String::from("Succeded"),
            PodStatusEnum::Failed => String::from("Failed"),
            PodStatusEnum::Unknown => String::from("Unknown"),
            PodStatusEnum::Pending => String::from("Pending"),
        }
    }
}

#[async_trait]
impl Waitable for WaitableKubernetesResource<Api<Pod>> {

    type StatusEnum = PodStatusEnum;

    async fn wait(&self, fields: &str, resource_status: Self::StatusEnum) -> Result<(), OrchestratorError> {
        let wp = WatchParams::default().fields(&fields)
        .timeout(std::env::var("K8S_WATCH_TIMEOUT")
            .unwrap_or("10".to_string()).parse().unwrap_or(10)
        );

        let mut stream = self.resource.watch(&wp, "0").await?.boxed();

        while let Some(status) = stream.try_next().await? {
            match status {
                WatchEvent::Modified(o) => {
                    if let Some(s) = o.status.as_ref(){
                        let phase = s.phase.clone().unwrap_or_default();
                        if phase.eq(&resource_status.to_string()) {
                            break;
                        }
                    }
                }
                WatchEvent::Error(e) => {
                    return Err(e.into())
                }
                _ => {}
            }
        }
        Ok(())
    }
}