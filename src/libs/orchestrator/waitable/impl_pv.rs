use axum::async_trait;
use futures::{StreamExt, TryStreamExt};
use k8s_openapi::api::core::v1::PersistentVolumeClaim;
use kube::{api::{WatchEvent, WatchParams}, Api};

use crate::libs::orchestrator::OrchestratorError;

use super::{Waitable, WaitableKubernetesResource};

#[async_trait]
impl Waitable for WaitableKubernetesResource<Api<PersistentVolumeClaim>> {
    async fn wait(&self, fields: &str, resource_status: &str) -> Result<(), OrchestratorError> {
        let wp = WatchParams::default().fields(&fields)
        .timeout(std::env::var("K8S_WATCH_TIMEOUT")
            .unwrap_or("10".to_string()).parse().unwrap_or(10)
        );

        let mut stream = self.resource.watch(&wp, "0").await?.boxed();

        while let Some(status) = stream.try_next().await? {
            match status {
                WatchEvent::Modified(o) => {
                    if let Some(s) = o.status.as_ref(){
                        let phase = s.phase.clone().unwrap_or_default();
                        println!("phase: {}", phase);
                        if phase.eq(resource_status) {
                            break;
                        }
                    }
                }
                WatchEvent::Error(e) => {
                    return Err(e.into())
                }
                _ => {}
            }
        }
        Ok(())
    }
}