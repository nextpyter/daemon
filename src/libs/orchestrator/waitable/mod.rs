use std::fmt::Display;

use axum::async_trait;
use super::OrchestratorError;
// mod impl_pv;
pub mod impl_pod;

#[async_trait]
/// we need to wait for resources in kubernetes, since things are not "sequential"
/// like in docker environments.
pub trait Waitable {
    type StatusEnum: ToString;
    async fn wait(&self, fields: &str, status: Self::StatusEnum) -> Result<(), OrchestratorError>;
}

pub struct WaitableKubernetesResource<R> {
    pub resource: R,
}
impl<R> WaitableKubernetesResource<R> {
    pub fn new(resource: R) -> Self {
        WaitableKubernetesResource {
            resource
        }
    } 
}