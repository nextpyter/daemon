use crate::libs::types::notebook_description::NotebookDescriptionConversionError;

use super::OrchestratorError;

impl From<bollard::errors::Error> for OrchestratorError {
    fn from(value: bollard::errors::Error) -> Self {
        OrchestratorError::DockerError(value)
    }
}

impl From<NotebookDescriptionConversionError> for OrchestratorError {
    fn from(value: NotebookDescriptionConversionError) -> Self {
        OrchestratorError::Generic(value.to_string())
    }
}

impl From<serde_json::Error> for OrchestratorError {
    fn from(value: serde_json::Error) -> Self {
        OrchestratorError::SerializationError(value.to_string())
    }
}

impl From<kube::Error> for OrchestratorError {
    fn from(value: kube::Error) -> Self {
        OrchestratorError::KubernetesError(value.to_string())
    }
}

impl From<kube::core::ErrorResponse> for OrchestratorError {
    fn from(value: kube::core::ErrorResponse) -> Self {
        OrchestratorError::KubernetesError(format!("waiting error: {}", value.to_string()))
    }
}