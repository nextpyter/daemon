use thiserror::Error;

pub enum ImagePullJobStatus {
    Pulling,
    Errored,
    Pulled,
}

#[derive(Error, Debug)]
pub enum ImagePullJobStatusParseError {
    #[error("unknown status")]
    UnknownStatus
}

impl Into<String> for ImagePullJobStatus {
    fn into(self) -> String {
        match self {
            ImagePullJobStatus::Pulling => "pulling".to_string(),
            ImagePullJobStatus::Errored => "errored".to_string(),
            ImagePullJobStatus::Pulled => "pulled".to_string(),
        }
    }
}

impl TryFrom<String> for ImagePullJobStatus {    
    type Error = ImagePullJobStatusParseError;
    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.as_str() {
            "pulling" => Ok(ImagePullJobStatus::Pulling),
            "errored" => Ok(ImagePullJobStatus::Errored),
            "pulled" => Ok(ImagePullJobStatus::Pulled),
            _ => Err(ImagePullJobStatusParseError::UnknownStatus)
        }
    }    
}