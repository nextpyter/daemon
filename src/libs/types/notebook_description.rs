use bollard::secret::ContainerSummary;
use k8s_openapi::api::core::v1::Pod;
use kube::Resource;
use serde::{Deserialize, Serialize};
use thiserror::Error;
use utoipa::ToSchema;

use crate::libs::orchestrator::build_notebook_uri;

#[derive(Serialize, Deserialize, ToSchema)]
pub struct NotebookDescription {
    #[schema(example = "05d70bd2894a9a267611bb6519afd0f42af0c19996ce1daf414ce29ad3ff3296")]
    pub notebook_id: String,
    #[schema(example = "http://proxy-uri:8080/notebook/notebook-hfxKMA4gvZ-74l7ZC3b3Wnym")]
    pub notebook_endpoint: String,
    #[schema(example = "Up 2 seconds (health: starting)")]
    pub notebook_status: String,
    #[schema(example = "notebook-hfxKMA4gvZ-74l7ZC3b3Wnym")]
    pub notebook_name: String,
}
#[derive(Error, Debug)]
pub enum NotebookDescriptionConversionError {
    #[error("container name is empty")]
    EmptyNameError,
    #[error("container id is not defined")]
    NoIdError,
    #[error("container names are not defined")]
    NoNamesError,
    #[error("pod status is not defined")]
    NoPodStatusError
}

impl TryInto<NotebookDescription> for Pod {
    type Error = NotebookDescriptionConversionError;

    fn try_into(self) -> Result<NotebookDescription, Self::Error> {

        if let Some(id) = &self.meta().name {
            if let Some(status) = &self.status {
                return Ok(NotebookDescription {
                    notebook_id: id.to_string(),
                    notebook_name: id.to_string(),
                    notebook_endpoint: build_notebook_uri(&id, None),
                    notebook_status: status.phase.clone().unwrap_or("unknown".to_string())
                })
            }
            return Err(NotebookDescriptionConversionError::NoPodStatusError)
        }
        Err(NotebookDescriptionConversionError::NoIdError)
        
    }
}

impl TryFrom<ContainerSummary> for NotebookDescription {

    type Error = NotebookDescriptionConversionError;
    
    fn try_from(container: ContainerSummary) -> Result<Self, Self::Error> {

        if let Some(id) = container.id.as_ref() {
            if let Some(names) = container.names.as_ref() {
                if let Some(notebook_name) = names.first() {
                    let notebook_name = &notebook_name[1..].to_string();
                    return Ok(NotebookDescription { 
                        notebook_id: id.to_string(),
                        notebook_name: notebook_name.clone(),
                        notebook_endpoint: build_notebook_uri(&notebook_name, None),
                        notebook_status: container.status.clone().unwrap_or("unknown".to_string()),
                    })
                }
                return Err(NotebookDescriptionConversionError::EmptyNameError)            
            }
            return Err(NotebookDescriptionConversionError::NoNamesError)
        }
        Err(NotebookDescriptionConversionError::NoIdError)

    }
    
} 

impl TryFrom<&ContainerSummary> for NotebookDescription {

    type Error = NotebookDescriptionConversionError;
    
    fn try_from(container: &ContainerSummary) -> Result<Self, Self::Error> {

        if let Some(id) = container.id.as_ref() {
            if let Some(names) = container.names.as_ref() {
                if let Some(notebook_name) = names.first() {
                    let notebook_name = &notebook_name[1..].to_string();
                    return Ok(NotebookDescription { 
                        notebook_id: id.to_string(),
                        notebook_name: notebook_name.clone(),
                        notebook_endpoint: build_notebook_uri(&notebook_name, None),
                        notebook_status: container.status.clone().unwrap_or("unknown".to_string()),
                    })
                }
                return Err(NotebookDescriptionConversionError::EmptyNameError)            
            }
            return Err(NotebookDescriptionConversionError::NoNamesError)
        }
        Err(NotebookDescriptionConversionError::NoIdError)

    }
    
} 