use tracing::info;
use tracing_subscriber::fmt::format::FmtSpan;

pub mod libs;
mod log_util;
pub mod web;

static NANOID_ALPHABET: [char; 35] = [
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e',
    'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
    'u', 'v', 'x', 'y', 'z',
];

// TODO: cli

#[tokio::main]
async fn main() {
    let subscriber = tracing_subscriber::fmt()
        .compact()
        .with_span_events(FmtSpan::ENTER | FmtSpan::CLOSE)
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .expect("cannot set subscriber");

    let router = web::build_app().await;
    info!("starting axum listener...");
    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    info!("axum listener bound!");
    axum::serve(listener, router).await.unwrap();
}
