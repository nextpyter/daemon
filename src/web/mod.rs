pub mod errors;
pub mod extractors;
pub mod middlewares;
mod routes;
mod app_state;
pub mod dto;

use std::env;

use axum::{http::HeaderMap, response::IntoResponse, routing::get, Router};
use models::container_images::ContainerImage;
use tracing::info;
use utoipa::{openapi::security::{Http, HttpAuthScheme, SecurityScheme}, Modify, OpenApi};

use crate::{libs::types::notebook_description::NotebookDescription, web::{app_state::AppState, dto::{claims::user_claims::UserClaims, request::{delete_notebook_params::DeleteNotebookParams, get_image_logs_params::GetImageLogsParams, get_notebook_params::GetNotebookParams, pull_image_request::PullImageRequest, restart_notebook_params::RestartNotebookParams, spawn_container_request::SpawnNotebookRequest, stop_notebook_params::StopNotebookParams, Pagination}, response::{generic_success_response::GenericSuccessResponse, get_all_notebooks_response::GetAllNotebooksResponse, get_image_logs_response::GetImageLogsResponse, get_images_response::GetImagesResponse, get_notebook_response::GetNotebookResponse, pull_image_response::PullImageResponse, spawn_notebook_response::SpawnNotebookResponse}}, routes::{cfg::cfg_routes, containers::container_routes}}};

pub async fn build_app() -> Router {

    let state = AppState::new(&env::var("ORCHESTRATOR").unwrap()).await;
    info!("state ok");
    #[derive(OpenApi)]
    #[openapi(
        info(description = "Nextpyter Daemon HTTP endpoints. Used to interact with the underlying Docker/Kubernetes environment."),
        paths(
            routes::containers::root::spawn_notebook,
            routes::containers::root::restart_notebook,
            routes::containers::root::get_notebook,
            routes::containers::root::delete_notebook,
            routes::containers::root::get_all_notebooks,
            routes::cfg::root::request_image_pull,
            routes::cfg::root::get_image_logs,
            routes::cfg::root::get_images,
        ), 
        modifiers(&SecurityAddon),
        components(
            schemas(
                SpawnNotebookRequest,
                SpawnNotebookResponse, 
                UserClaims,
                StopNotebookParams,
                GenericSuccessResponse,
                RestartNotebookParams,
                GetNotebookParams,
                GetNotebookResponse,
                DeleteNotebookParams,
                NotebookDescription,
                GetAllNotebooksResponse,
                PullImageRequest,
                PullImageResponse,
                GetImageLogsParams,
                GetImageLogsResponse,
                GetImagesResponse,
                Pagination,
                ContainerImage,
            )
        )
    )]
    struct ApiDoc;
    struct SecurityAddon;
    
    impl Modify for SecurityAddon {
        fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
            let components: &mut utoipa::openapi::Components = openapi.components.as_mut().unwrap(); 
            components.add_security_scheme(
                "bearerAuth",
                SecurityScheme::Http(Http::new(HttpAuthScheme::Bearer)),
            )
        }
    }

    async fn build_json_schema() -> impl IntoResponse {

        let str_docs = ApiDoc::openapi()
            .to_pretty_json()
            .unwrap();
        let mut headers = HeaderMap::new();
        headers.insert("Content-Type", "application/json".parse().unwrap());

        (headers, str_docs)
    
    }
    
    let app = Router::new()
        .route("/", get(routes::main::root::index))
        .route("/json-schema", get(build_json_schema))
        .with_state(state.clone())
        .merge(container_routes(state.clone()))
        .merge(cfg_routes(state.clone()));

    info!("routes built correctly!");

    app
}
