use std::collections::HashMap;

use axum::{
    Json,
    extract::rejection::JsonRejection,
    http::StatusCode,
    response::{IntoResponse, Response},
};
use models::{ModelError, executors::ExecutorError};
use redis::RedisError;
use rqlite_rs::error::{QueryBuilderError, RequestError};
use serde_json::json;
use tokio::task::JoinError;
use validator::{ValidationErrors, ValidationErrorsKind};

use crate::libs::{
    orchestrator::OrchestratorError,
    types::notebook_description::NotebookDescriptionConversionError,
};

pub enum HttpError {
    ParsingError(String, StatusCode),
    InvalidFieldsError(HashMap<&'static str, ValidationErrorsKind>),
    Simple(StatusCode, String),
    DatabaseError,
}

impl HttpError {
    pub fn container_not_found_error() -> Self {
        HttpError::Simple(
            StatusCode::NOT_FOUND,
            "container_not_found".to_string(),
        )
    }
}

impl IntoResponse for HttpError {
    fn into_response(self) -> Response {
        let tuple_response = match self {
            HttpError::ParsingError(text, _) => (
                StatusCode::BAD_REQUEST,
                Json(json!({"success": false, "error": text})),
            ),
            HttpError::InvalidFieldsError(err) => {
                let invalid_fields: Vec<&str> = err.into_keys().collect();
                (
                    StatusCode::BAD_REQUEST,
                    Json(
                        json!({"success": false, "error": "invalid_fields", "fields": invalid_fields}),
                    ),
                )
            }
            HttpError::Simple(code, msg) => {
                (code, Json(json!({ "success": false, "error": msg })))
            }
            HttpError::DatabaseError => (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({ "success": false, "error": "fatal_error" })),
            ),
        };

        tuple_response.into_response()
    }
}

impl From<JsonRejection> for HttpError {
    // error while parsing invalid json
    fn from(err: JsonRejection) -> Self {
        Self::ParsingError("invalid_body".to_owned(), err.status())
    }
}

impl From<ValidationErrors> for HttpError {
    // error when validating structs
    fn from(err: ValidationErrors) -> Self {
        Self::InvalidFieldsError(err.into_errors())
    }
}
impl From<JoinError> for HttpError {
    // this is tokio's blocking task error
    fn from(_: JoinError) -> Self {
        Self::Simple(
            StatusCode::INTERNAL_SERVER_ERROR,
            "async_error".to_string(),
        )
    }
}
impl From<jsonwebtoken::errors::Error> for HttpError {
    fn from(_e: jsonwebtoken::errors::Error) -> Self {
        Self::Simple(StatusCode::INTERNAL_SERVER_ERROR, "bad_jwt".to_string())
    }
}
impl From<bollard::errors::Error> for HttpError {
    fn from(e: bollard::errors::Error) -> Self {
        Self::Simple(StatusCode::INTERNAL_SERVER_ERROR, e.to_string())
    }
}
impl From<NotebookDescriptionConversionError> for HttpError {
    fn from(e: NotebookDescriptionConversionError) -> Self {
        Self::Simple(StatusCode::INTERNAL_SERVER_ERROR, e.to_string())
    }
}

impl From<QueryBuilderError> for HttpError {
    fn from(error: QueryBuilderError) -> Self {
        Self::Simple(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("bad_query_fmt: {}", error),
        )
    }
}
impl From<RequestError> for HttpError {
    fn from(error: RequestError) -> Self {
        Self::Simple(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("bad_rqlite_request: {}", error),
        )
    }
}
impl From<RedisError> for HttpError {
    fn from(error: RedisError) -> Self {
        Self::Simple(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("bad_redis: {}", error),
        )
    }
}
impl From<ExecutorError> for HttpError {
    fn from(error: ExecutorError) -> Self {
        HttpError::Simple(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("bad_executor: {}", error),
        )
    }
}

impl From<ModelError> for HttpError {
    fn from(err: ModelError) -> Self {
        dbg!(&err);
        HttpError::DatabaseError
    }
}
impl From<OrchestratorError> for HttpError {
    fn from(err: OrchestratorError) -> Self {
        dbg!(&err);
        HttpError::Simple(
            StatusCode::INTERNAL_SERVER_ERROR,
            "bad_orchestrator".to_string(),
        )
    }
}

