use std::{env, sync::Arc};

use rqlite_rs::{RqliteClient, RqliteClientBuilder};

use crate::libs::orchestrator::{
    docker::DockerOrchestrator, kubernetes::KubernetesOrchestrator,
    Orchestrator,
};

#[derive(Clone)]
pub struct AppState {
    pub orchestrator: Arc<Box<dyn Orchestrator>>,
    pub rqlite: Arc<RqliteClient>,
    pub redis: Option<redis::Client>, // used to request image pulls from docker hub
    pub images_stream_name: Option<String>, // the stream name we want to publish image pull events on,
    pub notebook_base_url: String,
}

pub async fn get_orchestrator(name: &str) -> Box<dyn Orchestrator> {
    match name {
        "kubernetes" => Box::new(KubernetesOrchestrator::new().await),
        "docker" => {
            // this orchestartor connects to the proxied docker host if the DOCKER_HOST variable
            // is specified
            Box::new(DockerOrchestrator::new(env::var("DOCKER_HOST").is_ok()))
        }
        _ => panic!("no orchestrator found with that name!"),
    }
}
impl AppState {
    pub async fn new(orchestrator_name: &str) -> Self {
        let rqlite = RqliteClientBuilder::new()
            .known_host(
                env::var("CONFIG_STORE_HOST")
                    .unwrap_or("config-store:4001".to_string()),
            )
            .auth(
                env::var("RQLITE_USER").unwrap().as_str(),
                env::var("RQLITE_PASSWORD").unwrap().as_str(),
            )
            .build()
            .expect("cannot create rqlite client");

        let images_stream_name = {
            if let Ok(res) = env::var("IMAGES_STREAM_NAME") {
                Some(res)
            } else {
                None
            }
        };

        let mut redis = None;
        if orchestrator_name.eq("docker") {
            redis = Some(
                redis::Client::open(env::var("REDIS_URI").unwrap())
                    .expect("cannot connect to redis"),
            );
            assert!(images_stream_name.is_some());
        }

        AppState {
            orchestrator: Arc::new(get_orchestrator(orchestrator_name).await),
            rqlite: Arc::new(rqlite),
            redis,
            images_stream_name,
            notebook_base_url: env::var("NOTEBOOK_BASE_URL")
                .unwrap_or("/notebook".to_string()),
        }
    }
}
