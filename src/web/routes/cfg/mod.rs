use axum::{routing::{get, post}, Router};
use crate::web::AppState;
pub mod root;

pub fn cfg_routes(state: AppState) -> Router{

    Router::new()
        .route("/cfg/request-image-pull", post(root::request_image_pull))
        .route("/cfg/image-logs/:image_id", get(root::get_image_logs))
        .route("/cfg/images", get(root::get_images))
        .route("/cfg/images-no-paginate", get(root::get_images_no_paginate))
        .with_state(state)

}