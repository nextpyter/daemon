use crate::web::{
    AppState,
    dto::{
        request::{
            Pagination, get_image_logs_params::GetImageLogsParams,
            pull_image_request::PullImageRequest,
        },
        response::{
            get_image_logs_response::GetImageLogsResponse,
            get_images_response::{GetAllImagesResponse, GetImagesResponse},
            pull_image_response::PullImageResponse,
        },
    },
    errors::HttpError,
    extractors::validate_body::ValidatedJson,
};
use axum::{
    Json,
    extract::{Path, Query, State},
};
use models::{
    container_images::{ContainerImage, ContainerImages},
    container_images_logs::{ContainerImageLogStatus, ContainerImagesLogs},
};
use nanoid::nanoid;

/// This route sends an event on a redis stream which is listened by pagletto (a redis consumer).
/// You can poll the status of the event with the `image_id` field that is returned
#[utoipa::path(
    post,
    path = "/cfg/request-image-pull",
    responses(
        (
            status = 200, description = "Request issued successfully", body = PullImageResponse,
        )
    ),
    security(
        ("bearerAuth" = [])
    ),
    request_body(
        content_type = "application/json", content = PullImageRequest
    )
)]
pub async fn request_image_pull(
    State(s): State<AppState>,
    ValidatedJson(body): ValidatedJson<PullImageRequest>,
) -> Result<Json<PullImageResponse>, HttpError> {
    let PullImageRequest {
        image_name,
        image_tag,
    } = body;

    let image_id = nanoid!(32);

    if s.redis.is_none() {
        // kubernetes :)
        let rqlite = s.rqlite;
        ContainerImage::insert_requested(
            &rqlite,
            &image_id,
            &image_name,
            &image_tag.clone().unwrap_or("latest".to_string()),
        )
        .await?;

        ContainerImagesLogs::append_log_for_image(
            &rqlite,
            &image_id,
            ContainerImageLogStatus::Done,
            Some("image pulled correctly"),
        )
        .await?;
    } else {
        let mut conn =
            s.redis.unwrap().get_multiplexed_async_connection().await?;

        let _: () = redis::Cmd::xadd(
            s.images_stream_name.unwrap(),
            "*",
            &[
                ("image_name", &image_name),
                ("image_tag", &image_tag.unwrap_or(String::from("latest"))),
                ("job_id", &image_id),
            ],
        )
        .query_async(&mut conn)
        .await?;
    }

    Ok(Json(PullImageResponse {
        success: true,
        image_id,
    }))
}

/// This route gets information about a container image. Info such as pull status, errors and more can be retrieved from here.
#[utoipa::path(
    get,
    path = "/cfg/image-logs/{id}",
    responses(
        (
            status = 200, description = "Logs obtained successfully", body = GetImageLogsResponse,
        )
    ),
    security(
        ("bearerAuth" = [])
    ),
    request_body(
        content_type = "application/json", content = PullImageRequest
    )
)]
pub async fn get_image_logs(
    State(s): State<AppState>,
    Path(GetImageLogsParams { image_id }): Path<GetImageLogsParams>,
) -> Result<Json<GetImageLogsResponse>, HttpError> {
    let result = ContainerImage::get_logs(&s.rqlite, &image_id).await?;

    Ok(Json(GetImageLogsResponse {
        success: true,
        result,
    }))
}

/// This route returns all images available.
///
/// You can paginate the results using the `page` query parameter.
#[utoipa::path(
    get,
    path = "/cfg/images",
    params(Pagination),
    responses(
        (
            status = 200, description = "Request issued successfully", body = GetImagesResponse,
        )
    ),
    security(
        ("bearerAuth" = [])
    ),
)]
pub async fn get_images(
    State(s): State<AppState>,
    Query(Pagination { page }): Query<Pagination>,
) -> Result<Json<GetImagesResponse>, HttpError> {
    let conn = s.rqlite.clone();
    let result = ContainerImages::paginate(&conn, page).await?;
    let pages: i64 =
        (ContainerImages::count(&conn).await? as f64 / 20.0).ceil() as i64;

    Ok(Json(GetImagesResponse {
        success: true,
        result,
        pages,
    }))
}

/// This route returns all images available.
///
/// You can paginate the results using the `page` query parameter.
#[utoipa::path(
    get,
    path = "/cfg/images-no-paginate",
    params(Pagination),
    responses(
        (
            status = 200, description = "Request issued successfully", body = GetImagesResponse,
        )
    ),
    security(
        ("bearerAuth" = [])
    ),
)]
pub async fn get_images_no_paginate(
    State(s): State<AppState>,
) -> Result<Json<GetAllImagesResponse>, HttpError> {
    let conn = s.rqlite.clone();
    let result = ContainerImages::get_all(&conn).await?;

    Ok(Json(GetAllImagesResponse {
        success: true,
        result,
    }))
}

