use axum::{
    Router,
    routing::{delete, get, post},
};

use crate::web::AppState;

pub mod root;

pub fn container_routes(state: AppState) -> Router {
    Router::new()
        .route("/containers", get(root::index))
        .route(
            "/containers/stop/notebook/:container_name",
            post(root::stop_notebook),
        )
        .route(
            "/containers/restart/notebook/:container_name",
            post(root::restart_notebook),
        )
        .route(
            "/containers/notebook/:container_name",
            get(root::get_notebook),
        )
        .route(
            "/containers/notebook/:container_name",
            delete(root::delete_notebook),
        )
        .route("/containers/notebook", post(root::spawn_notebook))
        .route("/containers/notebooks", get(root::get_all_notebooks))
        .with_state(state)
}

