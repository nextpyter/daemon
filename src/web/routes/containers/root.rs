use axum::{
    Json,
    extract::{Path, State},
    http::StatusCode,
};
use models::container_images::ContainerImage;
use nanoid::nanoid;
use serde_json::{Value, json};

use crate::{
    NANOID_ALPHABET,
    libs::orchestrator::{build_notebook_name, build_volume_name},
    web::{
        AppState,
        dto::{
            request::{
                delete_notebook_params::DeleteNotebookParams,
                get_notebook_params::GetNotebookParams,
                restart_notebook_params::RestartNotebookParams,
                spawn_container_request::SpawnNotebookRequest,
                stop_notebook_params::StopNotebookParams,
            },
            response::{
                generic_success_response::GenericSuccessResponse,
                get_all_notebooks_response::GetAllNotebooksResponse,
                get_notebook_response::GetNotebookResponse,
                spawn_notebook_response::SpawnNotebookResponse,
            },
        },
        errors::HttpError,
        extractors::validate_body::ValidatedJson,
    },
};

pub async fn index() -> Result<Json<Value>, HttpError> {
    Ok(Json(json!({
        "success": true,
        "message": "/containers endpoints"
    })))
}

/// This route spawns a new notebook container given an image and a mount path.
///
/// Since the containers are proxied by an Nginx instance, the response will include everything needed
/// to connect to them, including a direct URI.
///
/// Note that the "image_id" parameter must exist in the "container_images" table inside the database, and is obtained by pulling a new image with the
/// corresponding endpoint.
#[utoipa::path(
    post,
    path = "/containers/notebook",
    responses(
        (
            status = 200, description = "Container spawned successfully", body = SpawnNotebookResponse,
            status = 404, description = "The specified container image does not exist inside the database",
        )
    ),
    security(
        ("bearerAuth" = [])
    ),
    request_body(
        content_type = "application/json", content = SpawnNotebookRequest
    )
)]
pub async fn spawn_notebook(
    State(s): State<AppState>,
    // Token(user): Token<Claim<UserClaims>>,
    ValidatedJson(body): ValidatedJson<SpawnNotebookRequest>,
) -> Result<Json<SpawnNotebookResponse>, HttpError> {
    let SpawnNotebookRequest {
        folder_path,
        filename,
        image_id,
    } = body;

    let container_name = build_notebook_name(&nanoid!(24, &NANOID_ALPHABET)); // notebook container name, can be used to retrieve the container itself
    let notebook_token = nanoid!(32, &NANOID_ALPHABET);

    let volume_name = s
        .orchestrator
        .create_notebook_volume(&container_name, &folder_path)
        .await?;

    // check if the image is allowed
    let container_image = ContainerImage::from_id(&s.rqlite, &image_id).await?;
    if container_image.is_none() {
        return Err(HttpError::Simple(
            StatusCode::NOT_FOUND,
            "container_image_not_found".to_string(),
        ));
    }
    let container_image = container_image.unwrap();

    let image_name =
        format!("{}:{}", &container_image.name, &container_image.tag);

    let container_endpoint = s
        .orchestrator
        .start_notebook_container(
            &container_name,
            &image_name,
            vec![
                "start-notebook.sh",
                "--allow-root", // this is beacuse we need to change permissions and then we'll change the user
                &format!("--IdentityProvider.token={}", notebook_token), // access token for the notebook
                &format!(
                    "--NotebookApp.base_url={}/{}",
                    s.notebook_base_url, container_name
                ), // needed to forward redirects correctly from within the notebook
                "--NotebookApp.allow_remote_access=true", // needed for proxy
                "--NotebookApp.trust_xheaders=true",      // needed for proxy
                "--NotebookApp.allow_origin='*'",         // needed for proxy
                "--NotebookApp.port=15750",
                //"--ServerApp.tornado_settings=$csp",
                //"--YDocExtension.log_level=40",
                "--ServerApp.root_dir=/home/nextpyter",
            ],
            &volume_name,
            filename.as_deref(),
        )
        .await?;

    Ok(Json(SpawnNotebookResponse {
        success: true,
        container_name,
        container_endpoint,
        notebook_token,
    }))
}

#[utoipa::path(
    post,
    path = "/containers/stop/notebook/{name}",
    responses(
        (status = 200, description = "Notebook stopped successfully", body = GenericSuccessResponse),
        (status = 404, description = "Notebook was not found")
    ),
    security(
        ("bearerAuth" = [])
    ),
    params(
        ("container_name" = String, Path, description = "Notebook name (basically its unique id)")
    )
)]
pub async fn stop_notebook(
    State(s): State<AppState>,
    // Token(user): Token<Claim<UserClaims>>,
    Path(StopNotebookParams { container_name }): Path<StopNotebookParams>,
) -> Result<Json<GenericSuccessResponse>, HttpError> {
    if s.orchestrator
        .get_notebook_container(&container_name)
        .await?
        .is_some()
    {
        s.orchestrator
            .stop_notebook_container(&container_name)
            .await?;
        Ok(Json(GenericSuccessResponse { success: true }))
    } else {
        Err(HttpError::container_not_found_error())
    }
}

/// This route restart a notebook spawned using the daemon, even if it's not running
#[utoipa::path(
    post,
    path = "/containers/restart/notebook/{name}",
    responses(
        (status = 200, description = "Notebook restarted successfully", body = RestartNotebookResponse),
        (status = 404, description = "Notebook was not found")
    ),
    security(
        ("bearerAuth" = [])
    ),
    params(
        ("container_name" = String, Path, description = "Notebook name (basically its unique id)")
    )
)]
pub async fn restart_notebook(
    State(s): State<AppState>,
    Path(RestartNotebookParams { container_name }): Path<RestartNotebookParams>,
) -> Result<Json<GenericSuccessResponse>, HttpError> {
    if s.orchestrator
        .get_notebook_container(&container_name)
        .await?
        .is_some()
    {
        s.orchestrator
            .restart_notebook_container(&container_name)
            .await?;

        Ok(Json(GenericSuccessResponse { success: true }))
    } else {
        Err(HttpError::container_not_found_error())
    }
}

/// This route outputs information about a notebook spawned using the daemon (even if it's not running).
#[utoipa::path(
    get,
    path = "/containers/notebook/{name}",
    responses(
        (status = 200, description = "Notebook info obtained successfully", body = GetNotebookResponse),
        (status = 404, description = "Notebook was not found")
    ),
    security(
        ("bearerAuth" = [])
    ),
    params(
        ("container_name" = String, Path, description = "Notebook name (basically its unique id)")
    )
)]
pub async fn get_notebook(
    State(s): State<AppState>,
    Path(GetNotebookParams { container_name }): Path<GetNotebookParams>,
) -> Result<Json<GetNotebookResponse>, HttpError> {
    if let Some(notebook) = s
        .orchestrator
        .get_notebook_container(&container_name)
        .await?
    {
        Ok(Json(GetNotebookResponse {
            success: true,
            notebook,
        }))
    } else {
        Err(HttpError::container_not_found_error())
    }
}

/// This route gets all the notebooks spawned using the daemon, even if they're not running
#[utoipa::path(
    get,
    path = "/containers/notebooks",
    responses(
        (status = 200, description = "Retrieves all notebooks started by the daemon", body = GetAllNotebooksResponse),
    ),
    security(
        ("bearerAuth" = [])
    ),
    params(
        // ("from" = String, Query, description = "Pagination key, the container name you want the pagination to start from")
    )
)]
pub async fn get_all_notebooks(
    State(s): State<AppState>,
    // Query(pagination): Query<Pagination>,
) -> Result<Json<GetAllNotebooksResponse>, HttpError> {
    // implement pagination (use a support db)
    let all_notebooks = s.orchestrator.get_all_notebook_containers_paginated(/*pagination.from.as_deref()*/).await?;

    Ok(Json(GetAllNotebooksResponse {
        success: true,
        count: all_notebooks.len(),
        containers: all_notebooks,
    }))
}

/// This route deletes a notebook and all data associated to it.
#[utoipa::path(
    delete,
    path = "/containers/notebook/{name}",
    responses(
        (status = 200, description = "Notebook deleted successfully", body = DeleteNotebookResponse),
        (status = 404, description = "Notebook was not found")
    ),
    security(
        ("bearerAuth" = [])
    ),
    params(
        ("container_name" = String, Path, description = "Notebook name (basically its unique id)")
    )
)]
pub async fn delete_notebook(
    State(s): State<AppState>,
    Path(DeleteNotebookParams { container_name }): Path<DeleteNotebookParams>,
) -> Result<Json<GenericSuccessResponse>, HttpError> {
    if let Some(notebook) = s
        .orchestrator
        .get_notebook_container(&container_name)
        .await?
    {
        s.orchestrator
            .stop_notebook_container(&notebook.notebook_name)
            .await?;
        s.orchestrator
            .delete_notebook_container(&notebook.notebook_name)
            .await?;
        s.orchestrator
            .delete_notebook_container_data(&build_volume_name(
                &notebook.notebook_name,
            ))
            .await?;

        Ok(Json(GenericSuccessResponse { success: true }))
    } else {
        Err(HttpError::container_not_found_error())
    }
}
