use serde::{Deserialize, Serialize};
use utoipa::ToSchema;
#[derive(Serialize, Deserialize, Clone, ToSchema)]
pub struct UserClaims {
    pub user_id: String
}