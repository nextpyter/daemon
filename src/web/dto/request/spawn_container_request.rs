use serde::Deserialize;
use utoipa::ToSchema;
use validator::Validate;

#[derive(Deserialize, Validate, ToSchema)]
pub struct SpawnNotebookRequest {
    #[schema(example = "/path/to/local/folder/you/want/to/bind")]
    pub folder_path: String,
    #[schema(example = "notebook.ipynb")]
    pub filename: Option<String>,
    #[schema(example = "jupyter/minimal-notebook")]
    pub image_id: String,
    /*#[into_params(description = "NextCloud user ID")]
    pub user_id: String,*/
    /*#[into_params(description = "Notebook folder ID")]
    pub folder_id: String,*/
}

