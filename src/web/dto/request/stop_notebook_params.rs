use serde::Deserialize;
use utoipa::ToSchema;

#[derive(Deserialize, ToSchema)]
pub struct StopNotebookParams {
    #[schema(example = "notebook-o1dutd2cc8tkgic06lh9osrp")]
    pub container_name: String,
}