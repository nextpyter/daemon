use serde::Deserialize;
use utoipa::ToSchema;

#[derive(Deserialize, ToSchema)]
pub struct GetImageLogsParams {
    #[schema(example = "32-char-id")]
    pub image_id: String,
}