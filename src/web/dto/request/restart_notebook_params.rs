use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

#[derive(Serialize, Deserialize, ToSchema)]
pub struct RestartNotebookParams {
    #[schema(example = "notebook-o1dutd2cc8tkgic06lh9osrp")]
    pub container_name: String,
}