use serde::Deserialize;
use utoipa::ToSchema;
use validator::Validate;

#[derive(Deserialize, Validate, ToSchema)]
pub struct PullImageRequest {
    #[schema(example = "jupyter/minimal-notebook")]
    pub image_name: String,
    #[schema(example = "latest")]
    pub image_tag: Option<String>,
}