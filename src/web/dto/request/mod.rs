use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};
use validator::Validate;

pub mod spawn_container_request;
pub mod stop_notebook_params;
pub mod restart_notebook_params;
pub mod get_notebook_params;
pub mod delete_notebook_params;
pub mod pull_image_request;
pub mod get_image_logs_params;

#[derive(Serialize, Deserialize, IntoParams, ToSchema, Validate)]
pub struct Pagination {
    #[validate(range(min = 1))]
    #[schema(example = 1)]
    pub page: i64,
}