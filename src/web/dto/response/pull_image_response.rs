use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

#[derive(Serialize, Deserialize, ToSchema)]
pub struct PullImageResponse {
    #[schema(example = true)]
    pub success: bool,
    #[schema(example = "32-char-string")]
    pub image_id: String,
}