use models::container_images::LogEntry;
use serde::Serialize;
use utoipa::ToSchema;

#[derive(ToSchema, Serialize)]
pub struct GetImageLogsResponse {
    pub success: bool,
    pub result: Vec<LogEntry>,
}

