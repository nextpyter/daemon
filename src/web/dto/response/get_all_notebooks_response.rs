use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

use crate::libs::types::notebook_description::NotebookDescription;

#[derive(Serialize, Deserialize, ToSchema)]
pub struct GetAllNotebooksResponse {
    pub success: bool,
    pub containers: Vec<NotebookDescription>,
    #[schema(example = 1)]
    pub count: usize,
}