use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

use crate::libs::types::notebook_description::NotebookDescription;

#[derive(Serialize, Deserialize, ToSchema)]
pub struct GetNotebookResponse {
    pub success: bool,
    pub notebook: NotebookDescription
}