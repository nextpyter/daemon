use serde::Serialize;
use utoipa::{ToResponse, ToSchema};

#[derive(Serialize, ToResponse, ToSchema)]
#[response(description="Object containing everything needed to connect to the notebook.")]
pub struct SpawnNotebookResponse {
    #[schema(example = true)]
    pub success: bool,
    #[schema(example = "notebook-hfxKMA4gvZ-74l7ZC3b3Wnym")]
    pub container_name: String,
    #[schema(example = "http://proxy-uri:8080/notebook/notebook-hfxKMA4gvZ-74l7ZC3b3Wnym")]
    pub container_endpoint: String,
    #[schema(example = "KwODErMig4oZFPAPuYbXG1ug26lSgp4-")]
    pub notebook_token: String,
}