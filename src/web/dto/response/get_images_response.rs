use models::container_images::PaginatedContainerImage;
use serde::Serialize;
use utoipa::ToSchema;

#[derive(ToSchema, Serialize)]
pub struct GetImagesResponse {
    pub success: bool,
    pub result: Vec<PaginatedContainerImage>,
    #[schema(example = 10)]
    pub pages: i64,
}

#[derive(ToSchema, Serialize)]
pub struct GetAllImagesResponse {
    pub success: bool,
    pub result: Vec<PaginatedContainerImage>,
}

