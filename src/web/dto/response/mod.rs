pub mod spawn_notebook_response;
pub mod get_notebook_response;
pub mod get_all_notebooks_response;
pub mod generic_success_response;
pub mod pull_image_response;
pub mod get_image_logs_response;
pub mod get_images_response;
