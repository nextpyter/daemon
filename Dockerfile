FROM rust:bookworm
RUN useradd --user-group --system --create-home --no-log-init dockerissimo
# RUN chown -R dockerissimo:dockerissimo /var/run/
WORKDIR /code
# RUN chown dockerissimo:dockerissimo -R /code
# USER dockerissimo
COPY ./src /code/src
COPY ./models /code/models
COPY ./Cargo.toml /code/Cargo.toml
RUN cargo install cargo-watch
RUN cargo build
# ENTRYPOINT ["./wait-for", "db:", "--", "nodemon"] # for dev purposes only