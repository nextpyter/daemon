const firstMigration = require('./schema-29-04-2024');
const { DataApiClient } = require('rqlite-js');

if(process.env.RQLITE_HOST === undefined){
    throw new Error("no rqlite client defined!");
}
if(process.env.RQLITE_USERNAME === undefined){
    throw new Error("no rqlite user defined!");
}
if(process.env.RQLITE_PASSWORD === undefined){
    throw new Error("no rqlite password defined!");
}

(async () => {
    const client = new DataApiClient(
        process.env.RQLITE_HOST,
        {
            authentication: { 
                username: process.env.RQLITE_USERNAME,
                password: process.env.RQLITE_PASSWORD,
            }
        }
    );
    await firstMigration.down(client);
    await firstMigration.up(client);

    await client.execute("PRAGMA foreign_keys = ON;"); // this enables foreign keys cluster-wide
})()