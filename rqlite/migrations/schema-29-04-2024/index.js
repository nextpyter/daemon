const containerImages = require("./container_images");
const containerImagesLogs = require("./container_images_logs");

module.exports = {
    up: async conn => {
        await containerImages.up(conn);
        await containerImagesLogs.up(conn);
    },
    down: async conn => {
        await containerImages.down(conn);
        await containerImagesLogs.down(conn);
    }
}