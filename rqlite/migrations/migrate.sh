source $(pwd)/.env

echo $NETWORK

docker build $(pwd) -t daemon_migrations_29_04_2024
docker run \
    --rm \
    -w /code \
    --env-file $(pwd)/.env \
    --network $NETWORK \
    daemon_migrations_29_04_2024

docker rmi daemon_migrations_29_04_2024

echo "migrations completed"